# provide a block for hashicorp vault
provider "vault" { }

variable "cred_backend" {
    default = "aws"
}
  
variable "cred_role_name" {
    default = "demorole"
}

#Generate Dynamic aws credentials
data "vault_aws_access_credentials" "creds" {
    backend = var.cred_backend
    role = var.cred_role_name
  
}

provider "aws" {
    region = "us-east-1"
    access_key = data.vault_aws_access_credentials.creds.access_key
    secret_key = data.vault_aws_access_credentials.creds.secret_key

}

#create a user using the dynamic aws credential generated

resource "aws_iam_user" "user" {
    name = "gideonas"

}