variable "aws_acess_key" { }
variable "aws_secret_key" { }

# provide a block for hashicorp vault
provider "vault" {}

#enable aws engine in vault and credentials for vault to communicate witth aws 
resource "vault_aws_secret_backend" "aws" {
    access_key = var.aws_acess_key
    secret_key = var.aws_secret_key

    default_lease_ttl_seconds = "120"
    max_lease_ttl_seconds = "120"
  
}

# create a role in vault, which will map to aws credential generated
resource "vault_aws_secret_backend_role" "admin" {
    backend = vault_aws_secret_backend.aws.path
    name = "demorole"
    credential_type = "iam_user"
    policy_document = <<EOF
 {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "iam:*"
            "Resource": "*"
        }
    ]
 }
 EOF
}

output "backend" {
    value = vault_aws_secret_backend.aws.path
}
  
output "role" {
    value = vault_aws_secret_backend_role.admin.name